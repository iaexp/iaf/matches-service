package main

import (
	"flag"
	pb "gitlab.com/iaexp/iaf/matches-service/matchespb"
	"google.golang.org/grpc"
	"log"
	"net"
	"gitlab.com/iaexp/iaf/matches-service/server"
)

var (
	serverHost  = flag.String("host", "0.0.0.0", "the host to listen for connections")
	serverPort  = flag.String("port", ":8003", "the port to listen for new clients")
	arangoHost	= flag.String("dbhost", "arangodb", "arangoDB host")
	arangoPort	= flag.String("dbport", "8001", "arangoDB host")
	arangoUser	= flag.String("dbuser", "root", "arangoDB user")
	arangoPassword = flag.String("dbpassword", "matches-password", "arangoDB password")
)

func main() {
	flag.Parse()

	server.InitDatabase(*arangoHost, *arangoPort, *arangoUser, *arangoPassword)

	lis, err := net.Listen("tcp", *serverPort)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	grpcServer := grpc.NewServer()
	serv := &server.MatchesServiceServer{}
	pb.RegisterMatchesServiceServer(grpcServer, serv)
	// determine whether to use TLS
	log.Println("Server started on: " + *serverPort)
	grpcServer.Serve(lis)
}
