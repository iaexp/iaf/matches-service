package server

import (
	"context"
	"github.com/arangodb/go-driver/agency"
	"github.com/grpc/grpc-go/status"
	pb "gitlab.com/iaexp/iaf/matches-service/matchespb"
	"google.golang.org/grpc/codes"
	"strconv"
	"strings"
)

type MatchesServiceServer struct{}

func (*MatchesServiceServer) GetOne(ctx context.Context, in *pb.GetOneRequest) (*pb.Match, error) {
	var match *pb.Match
	_, err := col(matchesColName).ReadDocument(ctx, in.MatchID, match)
	if err != nil {
		if _, ok := err.(agency.KeyNotFoundError); ok {
			return nil, status.Errorf(codes.NotFound, "match of key " + in.MatchID + " not found")
		}
		return nil, status.Errorf(codes.Internal, "failed reading match document", err)
	}
	return match, nil
}

func (*MatchesServiceServer) ListAll(ctx context.Context, in *pb.ListAllRequest) (*pb.ListAllResponse, error) {
	if in.PageSize < 0 {
		return nil, status.Errorf(codes.InvalidArgument, "page_size cannot be negative")
	}

	var pageSize int32
	if in.PageSize == 0 {
		pageSize = 10
	} else {
		pageSize = in.PageSize
	}

	tokenArgs := strings.Split(in.PageToken, ":")
	if len(tokenArgs) != 2 {
		return nil, status.Errorf(codes.InvalidArgument, "page token invalid (format must be like 'order_by:offset')")
	}

	orderBy := strings.ReplaceAll(tokenArgs[0], " ", "")
	offset := strings.ReplaceAll(tokenArgs[1], " ", "")

	query := "FOR doc IN matches " +
		"SORT " + orderBy + " " +
		"LIMIT " + offset + ", " + string(pageSize) + " RETURN doc"

	var matches []*pb.Match
	cursor, err := db.Query(ctx, query, make(map[string]interface{}))
	if err != nil {
		return nil, status.Errorf(codes.Internal, "failed processing match query", err)
	}
	defer cursor.Close()
	match := &pb.Match{}
	for cursor.HasMore() {
		_, err := cursor.ReadDocument(ctx, match)
		if err != nil {
			return nil, status.Errorf(codes.Internal, "failed reading match document", err)
		}
		matches = append(matches, match)
	}

	offint, err := strconv.Atoi(offset)
	newOffset := offint + len(matches)
	nextPageToken := orderBy + ":" + string(newOffset)
	out := &pb.ListAllResponse{
		Matches:       matches,
		NextPageToken: nextPageToken,
	}
	return out, nil
}

func (*MatchesServiceServer) CreateOne(ctx context.Context, in *pb.CreateOneRequest) (*pb.CreateOneResponse, error) {
	meta, err := col(matchesColName).CreateDocument(ctx, &in.Match)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "failed creating match document", err)
	}

	out := &pb.Match{}
	_, err = col(matchesColName).ReadDocument(ctx, meta.Key, *out)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "failed reading match document", err)
	}

	return &pb.CreateOneResponse{Match: out}, nil
}
