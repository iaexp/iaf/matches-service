FROM golang:1.12 as builder

ADD https://github.com/golang/dep/releases/download/v0.4.1/dep-linux-amd64 /usr/bin/dep
RUN chmod +x /usr/bin/dep

WORKDIR $GOPATH/src/gitlab.com/iaexp/iaf/matches-service
COPY Gopkg.lock Gopkg.toml ./
RUN dep ensure --vendor-only
COPY . ./
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix nocgo -o /start .

FROM scratch
COPY --from=builder /start .
ENTRYPOINT ["./start"]